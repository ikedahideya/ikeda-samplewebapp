package jp.co.webtechnology.fw.logger;


/**
 * Loggerインスタンスのファクトリインタフェース。
 *
 * @author WT H.Ikeda
 */
interface LoggerInstanceFactory {

	/**
	 * Loggerインスタンスを生成します。
	 * @param name 名前
	 * @return Loggerインスタンス
	 */
	Logger getLogger(String name);

}
