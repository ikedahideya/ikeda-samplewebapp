package jp.co.webtechnology.fw.sql;

import java.sql.Connection;


/**
 * データベースの種類を表す列挙型クラス。
 *
 * @author FSWeb H.Ikeda
 */
public final class DatabaseType {

	/** Oracle */
	public static final DatabaseType ORACLE = new DatabaseType("ORACLE", "OracleImpl");
	/** SQLServer */
	public static final DatabaseType SQLSERVER = new DatabaseType("SQLSERVER", "SQLServerImpl");
	/** Symfoware, PostgreSQL */
	public static final DatabaseType POSTGRESQL = new DatabaseType("POSTGRESQL", "PostgreSQLImpl");

	/** 未知のデータベース種類 */
	public static final DatabaseType UNKNOWN = new DatabaseType("UNKNOWN", null);

	/** データベース名 */
	private final String name;
	/** 固有データアクセスオブジェクトの接尾辞 */
	private final String daoSuffix;


	/**
	 * 列挙型のため外部からのインスタンス化を禁止します。
	 * @param name データベース名
	 * @param daoSuffix 固有データアクセスオブジェクトの接尾辞
	 */
	private DatabaseType(String name, String daoSuffix) {
		this.name = name;
		this.daoSuffix = daoSuffix;
	}


	/**
	 * 固有データアクセスオブジェクトの接尾辞を返します。
	 * @return 固有データアクセスオブジェクトの接尾辞
	 */
	public String getDaoSuffix() {
		return this.daoSuffix;
	}


	/**
	 * データベースコネクション conn からデータベース種類を解決します。
	 *
	 * @param conn データベースコネクション
	 * @return データベースの種類
	 */
	public static DatabaseType resolveDatabaseType(Connection conn) {
		if (conn instanceof ConnectionWrapper) {
			throw new IllegalArgumentException("生の型を入れてください");
		}

		String connClassName = conn.getClass().getName();
		if ("oracle.jdbc.driver.OracleDriver".equals(connClassName)) {
			return ORACLE;
		} else if ("com.microsoft.sqlserver.jdbc.SQLServerDriver".equals(connClassName)) {
			return SQLSERVER;
		} else if ("org.postgresql.Driver".equals(connClassName)) {
			return POSTGRESQL;
		} else {
			// 典型的なJDBCドライバでない場合、少し悪あがきする
			String connClassNameLowerCase = connClassName.toLowerCase();
			if (connClassNameLowerCase.indexOf("oracle") >= 0) {
				return ORACLE;
			} else if (connClassNameLowerCase.indexOf("sqlserver") >= 0) {
				return SQLSERVER;
			} else if (connClassNameLowerCase.indexOf("postgresql") >= 0) {
				return POSTGRESQL;
			}

			// どうしても解決できない場合
			return UNKNOWN;
		}
	}

	/**
	 * オブジェクトの文字列表現を返します。
	 * @return データベース名
	 */
	public String toString() {
		return this.name;
	}

}
