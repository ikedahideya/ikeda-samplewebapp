package jp.co.webtechnology.fw.logger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 * ログ出力の状態を管理するクラス。
 * 実行時にログレベルを変更することができます。
 *
 * @author WT H.Ikeda
 */
public final class LoggerManager {


	/**
	 * Loggerのタイプを表す列挙型クラス。
	 *
	 * @author WT H.Ikeda
	 */
	static final class LoggerType {

		/** コンソールに出力するシンプルなLogger */
		static final LoggerType CONSOLE = new LoggerType(
				"Console",
				"jp.co.webtechnology.fw.logger.ConsoleLogger$ConsoleLoggerInstanceFactory");

		/** java.util.logging */
		static final LoggerType JAVA_UTIL_LOGGING = new LoggerType(
				"java.util.logging",
				"jp.co.webtechnology.fw.logger.JavaUtilLoggingLogger$JavaUtilLoggingLoggerFactory");

		/** 全項目の配列 */
		private static final LoggerType[] ITEMS = {
			CONSOLE,
			JAVA_UTIL_LOGGING,
		};

		/** 識別名 */
		private final String name;
		/** ファクトリクラス名 */
		private final String instanceFactoryClassName;

		/**
		 * 列挙型クラスのため外部からのインスタンス化を禁止します。
		 *
		 * @param name 識別名
		 * @param instanceFactoryClassName Loggerインスタンスのファクトリのクラス名
		 */
		private LoggerType(String name, String instanceFactoryClassName) {
			this.name = name;
			this.instanceFactoryClassName = instanceFactoryClassName;
		}

		/**
		 * 識別名 name に対応する列挙型インスタンスを返します。
		 *
		 * @param name 識別名
		 * @return 列挙型インスタンス。見つからない場合は null。
		 */
		static LoggerType valueOf(String name) {
			for (int i = 0; i < ITEMS.length; i++) {
				if (ITEMS[i].name.equalsIgnoreCase(name)) {
					return ITEMS[i];
				}
			}
			return null;
		}

		// 2012/11/15 WT W.Kubo Change Start
//		/**
//		 * Loggerインスタンスのファクトリを新規作成します。
//		 *
//		 * @return Loggerインスタンスのファクトリ
//		 */
//		LoggerInstanceFactory newInstanceFactory() {
//			try {
//				// 不要なクラスのロードを抑制するため、リフレクションにより生成
//				return (LoggerInstanceFactory) Class.forName(this.instanceFactoryClassName).newInstance();
		/**
		 * Loggerインスタンスのファクトリを新規作成します。
		 *
		 * @param logConfig Logger設定ファイル名
		 * @return Loggerインスタンスのファクトリ
		 */
		LoggerInstanceFactory newInstanceFactory(String logConfig) {
			try {
				// 不要なクラスのロードを抑制するため、リフレクションにより生成
				Class<?> clazz = Class.forName(this.instanceFactoryClassName);
				Constructor<?> constructor = clazz.getConstructor(new Class[]{String.class});
				return (LoggerInstanceFactory) constructor.newInstance(new Object[]{logConfig});
				// 2012/11/15 WT W.Kubo Change End
			} catch (ClassNotFoundException e) {
				throw new AssertionError(e);
			} catch (InstantiationException e) {
				throw new AssertionError(e);
			} catch (IllegalAccessException e) {
				throw new AssertionError(e);
				// 2012/11/15 WT W.Kubo Add Start
			} catch (SecurityException e) {
				throw new AssertionError(e);
			} catch (NoSuchMethodException e) {
				throw new AssertionError(e);
			} catch (IllegalArgumentException e) {
				throw new AssertionError(e);
			} catch (InvocationTargetException e) {
				throw new AssertionError(e);
				// 2012/11/15 WT W.Kubo Add End
			}
		}

		/**
		 * 識別名を返します。
		 * @return 識別名
		 */
		public String toString() {
			return this.name;
		}
	}


	/** ログ設定ファイルのパス(クラスパス) */
	private static final String CONFIG_PATH = "logger_config.xml";
	/** ログ設定ファイルのタグ名(logger-type) */
	private static final String CONFIG_ELEMENT_LOGGER_TYPE = "logger-type";
	/** ログ設定ファイルのタグ名(log-level) */
	private static final String CONFIG_ELEMENT_LOG_LEVEL = "log-level";
	// 2012/11/14 WT W.Kubo Add Start
	/** ログ設定ファイルのタグ名(log-configuration) */
	private static final String CONFIG_ELEMENT_LOG_CONFIGURATION = "log-configuration";
	// 2012/11/14 WT W.Kubo Add End

	/** ログ設定パラメータLoggerTypeのキー名 */
	private static final String VMPARAM_LOGGER_TYPE = "jp.co.webtechnology.fw.logger.LoggerType";
	/** ログ設定パラメータLogLevelのキー名 */
	private static final String VMPARAM_LOG_LEVEL = "jp.co.webtechnology.fw.logger.LogLevel";

	/** シングルトンインスタンス */
	private static LoggerManager instance = new LoggerManager();

	/** Loggerインスタンスのファクトリ */
	private final LoggerInstanceFactory instanceFactory;
	/** 現在のログレベル */
	private volatile LogLevel logLevel;

	/**
	 * 新規インスタンスを構築します。
	 * シングルトンのため外部からのインスタンス化を禁止します。
	 */
	private LoggerManager() {
		String loggerTypeName = null;
		String logLevelName = null;
		// 2012/11/14 WT W.Kubo Add Start
		String logConfigName = null;
		// 2012/11/14 WT W.Kubo Add End

		// 設定ファイルの読み込み
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(CONFIG_PATH);
		if (is != null) {
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(is);
				Element rootElement = doc.getDocumentElement();

				NodeList loggerTypeNodeList = rootElement.getElementsByTagName(CONFIG_ELEMENT_LOGGER_TYPE);
				if (loggerTypeNodeList.getLength() == 1) {
					loggerTypeName = ((Element) loggerTypeNodeList.item(0)).getFirstChild().getNodeValue().trim();
				}

				NodeList logLevelNodeList = rootElement.getElementsByTagName(CONFIG_ELEMENT_LOG_LEVEL);
				if (logLevelNodeList.getLength() == 1) {
					logLevelName = ((Element) logLevelNodeList.item(0)).getFirstChild().getNodeValue().trim();
				}

				// 2012/11/14 WT W.Kubo Add Start
				NodeList logConfigNodeList = rootElement.getElementsByTagName(CONFIG_ELEMENT_LOG_CONFIGURATION);
				if (logConfigNodeList.getLength() == 1) {
					logConfigName = ((Element) logConfigNodeList.item(0)).getFirstChild().getNodeValue().trim();
				}
				// 2012/11/14 WT W.Kubo Add End
			} catch (ParserConfigurationException e) {
				// この時点ではまだLoggerができていないため標準出力に出す
				e.printStackTrace(System.out);
			} catch (SAXException e) {
				// この時点ではまだLoggerができていないため標準出力に出す
				e.printStackTrace(System.out);
			} catch (IOException e) {
				// この時点ではまだLoggerができていないため標準出力に出す
				e.printStackTrace(System.out);
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace(System.out);
				}
			}
		}

		// VM引数から取得
		if (loggerTypeName == null) {
			loggerTypeName = System.getProperty(VMPARAM_LOGGER_TYPE);
		}
		if (logLevelName == null) {
			logLevelName = System.getProperty(VMPARAM_LOG_LEVEL);
		}

		// 初期状態の設定
		LoggerType loggerType = LoggerType.valueOf(loggerTypeName);
		if (loggerType == null) {
			// デフォルト
			// 2012/11/15 WT W.Kubo Change Start
//			this.instanceFactory = LoggerType.CONSOLE.newInstanceFactory();
			this.instanceFactory = LoggerType.CONSOLE.newInstanceFactory(logConfigName);
			// 2012/11/15 WT W.Kubo Change End
		} else {
			// 2012/11/15 WT W.Kubo Change Start
//			this.instanceFactory = loggerType.newInstanceFactory();
			this.instanceFactory = loggerType.newInstanceFactory(logConfigName);
			// 2012/11/15 WT W.Kubo Change End
		}

		LogLevel logLevel = LogLevel.valueOf(logLevelName);
		if (logLevel == null) {
			// デフォルト
			this.setLogLevel(LogLevel.INFO);
		} else {
			this.setLogLevel(logLevel);
		}
	}


	/**
	 * インスタンスを返します。
	 *
	 * @return インスタンス
	 */
	public static LoggerManager getInstance() {
		return instance;
	}


	/**
	 * Loggerインスタンスのファクトリを返します。
	 * @return Loggerインスタンスのファクトリ
	 */
	LoggerInstanceFactory getLoggerInstanceFactory() {
		return this.instanceFactory;
	}

	/**
	 * ログレベルを設定します。
	 * この設定はリアルタイムに反映されます。
	 *
	 * @param logLevel ログレベル
	 */
	public void setLogLevel(LogLevel logLevel) {
		this.logLevel = logLevel;
	}

	/**
	 * ログレベルを返します。
	 * @return ログレベル
	 */
	public LogLevel getLogLevel() {
		return this.logLevel;
	}
}
