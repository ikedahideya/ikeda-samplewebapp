package jp.co.webtechnology.useraccount;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.webtechnology.fw.sql.Dao;
import jp.co.webtechnology.fw.sql.DataAccessException;
import jp.co.webtechnology.fw.sql.SqlExecutor;


// DB固有のSQL構文がない場合、本クラスを抽象化せず、DB毎の継承クラスを作らずに本クラスのみで運用することが可能です。
/**
 * USERACCOUNTテーブルのDaoクラスです。
 *
 * @author FSWeb S.Ogawa
 */
public abstract class UserAccountDao extends Dao {

	/**
	 * コンストラクタです。
	 * @param conn DBConnection
	 */
	public UserAccountDao(Connection conn) {
		super(conn);
	}

	// DB固有のSQL文がある場合の実装例です。今回は使用していません。
	/**
	 * UserAccountテーブルをロックします。
	 * @throws DataAccessException データベースアクセス中にエラーが発生した場合
	 */
	public abstract void lockTableUserAccount() throws DataAccessException;

	/**
	 * ユーザ情報を全件検索します。
	 * @return ユーザ情報リスト
	 * @throws  DataAccessException データベースアクセス中に例外が発生した場合
	 */
	public List<UserAccountDto> findAll() throws DataAccessException {
		SqlExecutor executor = super.newSqlExecutor();
		try {
			executor.append("SELECT * FROM USERACCOUNT");
			ResultSet rs =  executor.executeQuery();

			List<UserAccountDto> resultList = new ArrayList<UserAccountDto>();
			while (rs.next()) {
				UserAccountDto result = new UserAccountDto();
				result.setUserId(rs.getString("USERID"));
				result.setPassword(rs.getString("PASSWORD"));
				result.setUserName(rs.getString("USERNAME"));
				resultList.add(result);
			}

			return resultList;

		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			executor.closeResultSet();
			executor.closePreparedStatement();
		}
	}

	/**
	 * 主キーからユーザ情報を検索します。
	 * @param userId ユーザID
	 * @return ユーザ情報
	 * @throws DataAccessException データベースアクセス中にエラーが発生した場合
	 */
	public UserAccountDto findByPK(String userId) throws DataAccessException {
		SqlExecutor executor = super.newSqlExecutor();
		try {
			executor.append("SELECT * FROM USERACCOUNT WHERE USERID = ?");
			executor.setString(userId);
			ResultSet rs =  executor.executeQuery();

			if (!rs.next()) {
				return null;
			}

			UserAccountDto result = new UserAccountDto();
			result.setUserId(rs.getString("USERID"));
			result.setPassword(rs.getString("PASSWORD"));
			result.setUserName(rs.getString("USERNAME"));

			return result;
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			executor.closeResultSet();
			executor.closePreparedStatement();
		}
	}

	/**
	 * ユーザ情報を追加します。
	 * @param dto 追加するユーザ情報
	 * @return 追加件数
	 * @throws DataAccessException データベースアクセス中にエラーが発生した場合
	 */
	public int insert(UserAccountDto dto) throws DataAccessException {
		SqlExecutor executor = super.newSqlExecutor();
		try {
			String sql = "INSERT INTO USERACCOUNT(USERID, PASSWORD, USERNAME) VALUES(?,?,?)";

			executor.append(sql);
			executor.setString(dto.getUserId());
			executor.setString(dto.getPassword());
			executor.setString(dto.getUserName());

			return executor.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			executor.closePreparedStatement();
		}
	}

	/**
	 * ユーザ情報を更新します。
	 * @param dto 更新するユーザ情報
	 * @return 更新件数
	 * @throws DataAccessException データベースアクセス中にエラーが発生した場合
	 */
	public int update(UserAccountDto dto) throws DataAccessException {
		SqlExecutor executor = super.newSqlExecutor();
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE USERACCOUNT SET");

			if(dto.getPassword() != null){
				sql.append(" PASSWORD = ?,");
				executor.setString(dto.getPassword());
			}
			if(dto.getUserName() != null){
				sql.append(" USERNAME = ?,");
				executor.setString(dto.getUserName());
			}

			sql.deleteCharAt(sql.length()-1);// 末尾の","除去
			sql.append(" WHERE USERID = ?");
			executor.setString(dto.getUserId());

			executor.append(sql.toString());
			return executor.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			executor.closePreparedStatement();
		}
	}

	/**
	 * ユーザ情報を削除します。
	 * @param userId 削除するユーザ情報のユーザID
	 * @return 更新件数
	 * @throws DataAccessException データベースアクセス中にエラーが発生した場合
	 */
	public int delete(String userId) throws DataAccessException {
		SqlExecutor executor = super.newSqlExecutor();
		try {
			executor.append("DELETE FROM USERACCOUNT WHERE USERID = ?");
			executor.setString(userId);
			return executor.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			executor.closePreparedStatement();
		}
	}

}
