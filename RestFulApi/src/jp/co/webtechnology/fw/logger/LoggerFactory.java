package jp.co.webtechnology.fw.logger;


/**
 * <p>
 * Loggerを生成するファクトリクラス。
 * </p>
 * <p>
 * 以下の方法でLoggerインスタンスを取得します:
 * <pre>
 * <code>Logger logger = LoggerFactory.getLogger(クラス名.class);</code>
 * </pre>
 * 生成されるLoggerの実体は、設定ファイルにて設定することが可能です。
 * 以下の3つの実体を選択することができます。
 * <table border="1">
 *   <tr>
 *     <th>logger-type設定値<th><th>動作</th>
 *   </tr>
 *   <tr>
 *     <td>console<td><td>コンソールに出力するシンプルなLogger</td>
 *   </tr>
 *   <tr>
 *     <td>java.util.logging<td><td>処理をjava.util.loggingに委譲</td>
 *   </tr>
 *   <tr>
 *     <td>log4j<td><td>処理をlog4jに委譲</td>
 *   </tr>
 * </table>
 * 選択されたタイプのLogger以外のライブラリはクラスロードされないことを保証します。
 * 例えば、consoleを選択した場合には、java.util.logging、log4jのライブラリは不要です。
 * </p>
 * <p>
 * 設定ファイルは、クラスパスのルートに logger_config.xml というファイル名で配置する必要があります。
 * 設定ファイルの記述例を下記に示します:
 * <pre>
 * <code>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;
 * &lt;logger-config&gt;
 *     &lt;logger-type&gt;console&lt;/logger-type&gt;
 *     &lt;log-level&gt;INFO&lt;/log-level&gt;
 * &lt;/logger-config&gt;
 * </code>
 * </pre>
 * log-levelは出力するログレベルです。
 * このログレベル以上のログのみが出力されます。
 * このログレベルは全Logger一律の設定となっており、名前ごとに個別に設定することはできません。
 * {@link LoggerManager#setLogLevel(LogLevel)}により実行時にログレベルを変更することが可能です。
 * </p>
 * <p>
 * 設定ファイルが見つからない場合には、VMパラメータの設定を読み込みます。
 * 下記のパラメータ名がそれぞれlogger-type、log-levelに対応しています。
 * <ul>
 * <li>jp.co.webtechnology.dp.logger.LoggerType</li>
 * <li>jp.co.webtechnology.dp.logger.LogLevel</li>
 * </ul>
 * 設定ファイルが見つからない場合、VMパラメータ指定がない場合には、
 * デフォルト設定(console, INFO)が適用されます。)
 * </p>
 * <p>
 * 参考までに、java.util.loggingにおけるログレベルの対応関係は下記の通りです:
 * <table border="1">
 *   <tr>
 *     <th>本API<th><th>java.util.logging</th>
 *   </tr>
 *   <tr>
 *     <td>trace<td><td>FINEST</td>
 *   </tr>
 *   <tr>
 *     <td>debug<td><td>FINE</td>
 *   </tr>
 *   <tr>
 *     <td>info<td><td>INFO</td>
 *   </tr>
 *   <tr>
 *     <td>warn<td><td>WARNING</td>
 *   </tr>
 *   <tr>
 *     <td>error<td><td>SEVERE</td>
 *   </tr>
 *   <tr>
 *     <td>fatal<td><td>SEVERE</td>
 *   </tr>
 * </table>
 *
 * @author WT H.Ikeda
 */
public final class LoggerFactory {

	/**
	 * static クラスのためインスタンス化を禁止します。
	 */
	private LoggerFactory() {
	}


	/**
	 * Loggerを返します。
	 *
	 * @param name Loggerの名前
	 * @return Logger
	 */
	public static Logger getLogger(String name) {
		return LoggerManager.getInstance().getLoggerInstanceFactory().getLogger(name);
	}

	/**
	 * Loggerを返します。
	 *
	 * @param clazz Loggerの名前(クラス指定)
	 * @return Logger
	 */
	public static Logger getLogger(Class<?> clazz) {
		return LoggerManager.getInstance().getLoggerInstanceFactory().getLogger(clazz.getName());
	}

}
