package jp.co.webtechnology.fw.sql;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

import jp.co.webtechnology.fw.logger.Logger;
import jp.co.webtechnology.fw.logger.LoggerFactory;


/**
 * ResultSetのラッパークラス。
 *
 * @author FSWeb H.IKeda
 */
class ResultSetWrapper implements ResultSet {

	/** Logger */
	private static Logger logger = LoggerFactory.getLogger(ResultSetWrapper.class);

	/** ResultSet */
	private final ResultSet rs;
	/** データベースの種類 */
	private final DatabaseType databaseType;
	/** リソース解放状態 */
	private boolean isClosed = false;


	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param stmt PreparedStatement(ログ出力用)
	 * @param rs ResultSet
	 * @param databaseType データベースの種類
	 */
	public ResultSetWrapper(PreparedStatement stmt, ResultSet rs, DatabaseType databaseType) {
		this.rs = rs;
		this.databaseType = databaseType;

		if (logger.isTraceEnabled()) {
			// 共通部品外の行をピンポイント出力
			String openLocation = "?";
			StackTraceElement[] stacktraces = new Throwable().getStackTrace();
			for (int i = 0; i < stacktraces.length; i++) {
				String clazz = stacktraces[i].getClassName();
				if (clazz.startsWith("java.lang.") ||
						clazz.startsWith("jp.co.webtechnology.dp.sql.")) {
					continue;
				}

				openLocation = stacktraces[i].toString();
				break;
			}
			logger.trace("open[" + this.rs.getClass().getName() + "@" + this.rs.hashCode() + ",stmt=@" + stmt.hashCode() + "] at " + openLocation);
		}
	}


	/** {@inheritDoc} */
	public boolean next() throws SQLException {
		return this.rs.next();
	}

	/** {@inheritDoc} */
	public String getString(int columnIndex) throws SQLException {
		// Oracleには""がなくNULLになってしまう→getStringは""をnullと解釈して統一
		String value = this.rs.getString(columnIndex);
		if (this.databaseType == DatabaseType.ORACLE) {
			return value;
		} else {
			if ("".equals(value)) {
				return null;
			} else {
				return value;
			}
		}
	}

	/** {@inheritDoc} */
	public int getInt(int columnIndex) throws SQLException {
		return this.rs.getInt(columnIndex);
	}

	/** {@inheritDoc} */
	public long getLong(int columnIndex) throws SQLException {
		return this.rs.getLong(columnIndex);
	}

	/** {@inheritDoc} */
	public byte[] getBytes(int columnIndex) throws SQLException {
		return this.rs.getBytes(columnIndex);
	}

	/** {@inheritDoc} */
	public String getString(String columnLabel) throws SQLException {
		// Oracleには""がなくNULLになってしまう→getStringは""をnullと解釈して統一
		String value = this.rs.getString(columnLabel);
		if (this.databaseType == DatabaseType.ORACLE) {
			return value;
		} else {
			if ("".equals(value)) {
				return null;
			} else {
				return value;
			}
		}
	}

	/** {@inheritDoc} */
	public int getInt(String columnLabel) throws SQLException {
		return this.rs.getInt(columnLabel);
	}

	/** {@inheritDoc} */
	public long getLong(String columnLabel) throws SQLException {
		return this.rs.getLong(columnLabel);
	}

	/** {@inheritDoc} */
	public byte[] getBytes(String columnLabel) throws SQLException {
		return this.rs.getBytes(columnLabel);
	}

	/** {@inheritDoc} */
	public ResultSetMetaData getMetaData() throws SQLException {
		return this.rs.getMetaData();
	}

	/** {@inheritDoc} */
	public InputStream getBinaryStream(int columnIndex) throws SQLException {
		return this.rs.getBinaryStream(columnIndex);
	}

	/** {@inheritDoc} */
	public InputStream getBinaryStream(String columnLabel) throws SQLException {
		return this.rs.getBinaryStream(columnLabel);
	}

	/** {@inheritDoc} */
	public void close() throws SQLException {
		this.rs.close();
		this.isClosed = true;
	}


	/**
	 * ファイナライズ処理を実行します。
	 * リソースの解放漏れを検出して警告ログを出力します。
	 *
	 * @throws Throwable 処理中にエラーが発生した場合
	 */
	protected void finalize() throws Throwable {
		try {
			if (!this.isClosed) {
				logger.debug("LEAK![@" + this.rs.hashCode() + "]");
				this.close();
			}
		} finally {
			super.finalize();
		}
	}

	// 以降、使用想定外
	/** {@inheritDoc} */
	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean wasNull() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean getBoolean(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public byte getByte(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public short getShort(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public float getFloat(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public double getDouble(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public BigDecimal getBigDecimal(int columnIndex, int scale)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Date getDate(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Time getTime(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Timestamp getTimestamp(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public InputStream getAsciiStream(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public InputStream getUnicodeStream(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean getBoolean(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public byte getByte(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public short getShort(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public float getFloat(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public double getDouble(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public BigDecimal getBigDecimal(String columnLabel, int scale)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Date getDate(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Time getTime(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Timestamp getTimestamp(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public InputStream getAsciiStream(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public InputStream getUnicodeStream(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void clearWarnings() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public String getCursorName() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Object getObject(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Object getObject(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int findColumn(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Reader getCharacterStream(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Reader getCharacterStream(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isBeforeFirst() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isAfterLast() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isFirst() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isLast() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void beforeFirst() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void afterLast() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean first() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean last() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getRow() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean absolute(int row) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean relative(int rows) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean previous() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setFetchDirection(int direction) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getFetchDirection() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setFetchSize(int rows) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getFetchSize() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getType() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getConcurrency() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean rowUpdated() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean rowInserted() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean rowDeleted() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNull(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBoolean(int columnIndex, boolean x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateByte(int columnIndex, byte x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateShort(int columnIndex, short x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateInt(int columnIndex, int x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateLong(int columnIndex, long x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateFloat(int columnIndex, float x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateDouble(int columnIndex, double x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBigDecimal(int columnIndex, BigDecimal x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateString(int columnIndex, String x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBytes(int columnIndex, byte[] x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateDate(int columnIndex, Date x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateTime(int columnIndex, Time x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateTimestamp(int columnIndex, Timestamp x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateAsciiStream(int columnIndex, InputStream x, int length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBinaryStream(int columnIndex, InputStream x, int length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateCharacterStream(int columnIndex, Reader x, int length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateObject(int columnIndex, Object x, int scaleOrLength)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateObject(int columnIndex, Object x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNull(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBoolean(String columnLabel, boolean x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateByte(String columnLabel, byte x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateShort(String columnLabel, short x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateInt(String columnLabel, int x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateLong(String columnLabel, long x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateFloat(String columnLabel, float x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateDouble(String columnLabel, double x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBigDecimal(String columnLabel, BigDecimal x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateString(String columnLabel, String x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBytes(String columnLabel, byte[] x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateDate(String columnLabel, Date x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateTime(String columnLabel, Time x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateTimestamp(String columnLabel, Timestamp x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateAsciiStream(String columnLabel, InputStream x, int length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBinaryStream(String columnLabel, InputStream x, int length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateCharacterStream(String columnLabel, Reader reader,
			int length) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateObject(String columnLabel, Object x, int scaleOrLength)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateObject(String columnLabel, Object x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void insertRow() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateRow() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void deleteRow() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void refreshRow() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void cancelRowUpdates() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void moveToInsertRow() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void moveToCurrentRow() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Statement getStatement() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Object getObject(int columnIndex, Map<String, Class<?>> map)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Ref getRef(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Blob getBlob(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Clob getClob(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Array getArray(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Object getObject(String columnLabel, Map<String, Class<?>> map)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Ref getRef(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Blob getBlob(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Clob getClob(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Array getArray(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Date getDate(int columnIndex, Calendar cal) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Date getDate(String columnLabel, Calendar cal) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Time getTime(int columnIndex, Calendar cal) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Time getTime(String columnLabel, Calendar cal) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Timestamp getTimestamp(int columnIndex, Calendar cal)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Timestamp getTimestamp(String columnLabel, Calendar cal)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public URL getURL(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public URL getURL(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateRef(int columnIndex, Ref x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateRef(String columnLabel, Ref x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBlob(int columnIndex, Blob x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBlob(String columnLabel, Blob x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateClob(int columnIndex, Clob x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateClob(String columnLabel, Clob x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateArray(int columnIndex, Array x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateArray(String columnLabel, Array x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public RowId getRowId(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public RowId getRowId(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateRowId(int columnIndex, RowId x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateRowId(String columnLabel, RowId x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getHoldability() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isClosed() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNString(int columnIndex, String nString)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNString(String columnLabel, String nString)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNClob(int columnIndex, NClob nClob) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNClob(String columnLabel, NClob nClob)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public NClob getNClob(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public NClob getNClob(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateSQLXML(int columnIndex, SQLXML xmlObject)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateSQLXML(String columnLabel, SQLXML xmlObject)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public String getNString(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public String getNString(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Reader getNCharacterStream(int columnIndex) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Reader getNCharacterStream(String columnLabel) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNCharacterStream(int columnIndex, Reader x, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNCharacterStream(String columnLabel, Reader reader,
			long length) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateAsciiStream(int columnIndex, InputStream x, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBinaryStream(int columnIndex, InputStream x, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateCharacterStream(int columnIndex, Reader x, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateAsciiStream(String columnLabel, InputStream x, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBinaryStream(String columnLabel, InputStream x,
			long length) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateCharacterStream(String columnLabel, Reader reader,
			long length) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBlob(int columnIndex, InputStream inputStream, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBlob(String columnLabel, InputStream inputStream,
			long length) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateClob(int columnIndex, Reader reader, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateClob(String columnLabel, Reader reader, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNClob(int columnIndex, Reader reader, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNClob(String columnLabel, Reader reader, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNCharacterStream(int columnIndex, Reader x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNCharacterStream(String columnLabel, Reader reader)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateAsciiStream(int columnIndex, InputStream x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBinaryStream(int columnIndex, InputStream x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateCharacterStream(int columnIndex, Reader x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateAsciiStream(String columnLabel, InputStream x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBinaryStream(String columnLabel, InputStream x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateCharacterStream(String columnLabel, Reader reader)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBlob(int columnIndex, InputStream inputStream)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateBlob(String columnLabel, InputStream inputStream)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateClob(int columnIndex, Reader reader) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateClob(String columnLabel, Reader reader)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNClob(int columnIndex, Reader reader) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void updateNClob(String columnLabel, Reader reader)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public <T> T getObject(String columnLabel, Class<T> type)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

}