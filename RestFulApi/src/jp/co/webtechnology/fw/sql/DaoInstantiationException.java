package jp.co.webtechnology.fw.sql;


/**
 * データアクセスオブジェクトのインスタンス生成に失敗したことを表す例外クラス。
 *
 * @author FSWeb H.Ikeda
 */
public class DaoInstantiationException extends RuntimeException {

	/** serialVersionUID */
	private static final long serialVersionUID = -4584230540535702110L;


	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param message エラーメッセージ
	 */
	public DaoInstantiationException(String message) {
		super(message);
	}

	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param message エラーメッセージ
	 * @param cause 原因の例外
	 */
	public DaoInstantiationException(String message, Throwable cause) {
		super(message, cause);
	}

}
