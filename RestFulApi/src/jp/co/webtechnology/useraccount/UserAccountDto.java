package jp.co.webtechnology.useraccount;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * USERACCOUNTテーブルのDTOクラス
 *
 * @author FSWeb S.Ogawa
 */
@XmlRootElement
public class UserAccountDto {

	/** ユーザID */
	private String userId;

	/** パスワード */
	private String password;

	/** ユーザ名 */
	private String userName;

	/**
	 * ユーザIDを取得します。
	 * @return ユーザID
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * ユーザIDを設定します。
	 * @param userId ユーザID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * パスワードを取得します。
	 * @return パスワード
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * パスワードを設定します。
	 * @param password パスワード
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * ユーザ名を取得します。
	 * @return ユーザ名
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * ユーザ名を設定します。
	 * @param userName ユーザ名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

}
