package jp.co.webtechnology.fw.logger;


/**
 * ログレベルを表す列挙型クラス。
 *
 * @author WT H.Ikeda
 */
public final class LogLevel {

	/** 詳細なトレースメッセージを表すレベル */
	public static final LogLevel TRACE = new LogLevel("TRACE", true, true, true, true, true, true);
	/** トレースメッセージを表すレベル */
	public static final LogLevel DEBUG = new LogLevel("DEBUG", false, true, true, true, true, true);
	/** 情報メッセージを表すレベル */
	public static final LogLevel INFO = new LogLevel("INFO", false, false, true, true, true, true);
	/** 潜在的な問題を表すメッセージレベル */
	public static final LogLevel WARN = new LogLevel("WARN", false, false, false, true, true, true);
	/** 障害を表すメッセージレベル */
	public static final LogLevel ERROR = new LogLevel("ERROR", false, false, false, false, true, true);
	/** 重大な障害を表すメッセージレベル */
	public static final LogLevel FATAL = new LogLevel("FATAL", false, false, false, false, false, true);
	/** ログ出力をオフにするために使われる特殊なレベル */
	public static final LogLevel OFF = new LogLevel("OFF", false, false, false, false, false, false);

	/** 全項目の配列 */
	private static final LogLevel[] ITEMS = {
		TRACE,
		DEBUG,
		INFO,
		WARN,
		ERROR,
		FATAL,
		OFF,
	};

	/** 識別名 */
	private final String name;

	/** TRACEログの出力有無 */
	private final boolean isTraceEnabled;
	/** DEBUGログの出力有無 */
	private final boolean isDebugEnabled;
	/** INFOログの出力有無 */
	private final boolean isInfoEnabled;
	/** WARNログの出力有無 */
	private final boolean isWarnEnabled;
	/** ERRORログの出力有無 */
	private final boolean isErrorEnabled;
	/** FATALログの出力有無 */
	private final boolean isFatalEnabled;


	/**
	 * 列挙型クラスのため外部からのインスタンス化を禁止します。
	 *
	 * @param name 識別名
	 * @param isTraceEnabled TRACEログの出力有無
	 * @param isDebugEnabled DEBUGログの出力有無
	 * @param isInfoEnabled INFOログの出力有無
	 * @param isWarnEnabled WARNログの出力有無
	 * @param isErrorEnabled ERRORログの出力有無
	 * @param isFatalEnabled FATALログの出力有無
	 */
	private LogLevel(
			String name,
			boolean isTraceEnabled,
			boolean isDebugEnabled,
			boolean isInfoEnabled,
			boolean isWarnEnabled,
			boolean isErrorEnabled,
			boolean isFatalEnabled) {
		this.name = name;

		this.isTraceEnabled = isTraceEnabled;
		this.isDebugEnabled = isDebugEnabled;
		this.isInfoEnabled = isInfoEnabled;
		this.isWarnEnabled = isWarnEnabled;
		this.isErrorEnabled = isErrorEnabled;
		this.isFatalEnabled = isFatalEnabled;
	}

	/**
	 * 識別名 name に対応する列挙型インスタンスを返します。
	 *
	 * @param name 識別名
	 * @return 列挙型インスタンス。見つからない場合は null。
	 */
	public static LogLevel valueOf(String name) {
		for (int i = 0; i < ITEMS.length; i++) {
			if (ITEMS[i].name.equalsIgnoreCase(name)) {
				return ITEMS[i];
			}
		}
		return null;
	}

	/**
	 * 列挙型の一覧を返します。
	 * @return 列挙型の一覧
	 */
	public static LogLevel[] values() {
		return new LogLevel[] {
				TRACE,
				DEBUG,
				INFO,
				WARN,
				ERROR,
				FATAL,
				OFF,
			};
	}

	/**
	 * TRACEログの出力有無を返します。
	 * @return TRACEログの出力有無
	 */
	boolean isTraceEnabled() {
		return this.isTraceEnabled;
	}

	/**
	 * DEBUGログの出力有無を返します。
	 * @return DEBUGログの出力有無
	 */
	boolean isDebugEnabled() {
		return this.isDebugEnabled;
	}

	/**
	 * INFOログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	boolean isInfoEnabled() {
		return this.isInfoEnabled;
	}

	/**
	 * WARNログの出力有無を返します。
	 * @return WARNログの出力有無
	 */
	boolean isWarnEnabled() {
		return this.isWarnEnabled;
	}

	/**
	 * ERRORログの出力有無を返します。
	 * @return ERRORログの出力有無
	 */
	boolean isErrorEnabled() {
		return this.isErrorEnabled;
	}

	/**
	 * FATALログの出力有無を返します。
	 * @return FATALログの出力有無
	 */
	boolean isFatalEnabled() {
		return this.isFatalEnabled;
	}

	/**
	 * 識別名を返します。
	 * @return 識別名
	 */
	public String name() {
		return this.name;
	}

	/**
	 * 識別名を返します。
	 * @return 識別名
	 */
	public String toString() {
		return this.name;
	}

}
