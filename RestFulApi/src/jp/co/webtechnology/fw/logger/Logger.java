package jp.co.webtechnology.fw.logger;


/**
 * Loggerのインタフェース。
 * 詳細な使用方法については{@link LoggerFactory}を参照してください。
 *
 * @author WT H.Ikeda
 */
public interface Logger {

	/**
	 * TRACEログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	boolean isTraceEnabled();

	/**
	 * TRACEログを出力します。
	 * @param message メッセージ
	 */
	void trace(Object message);

	/**
	 * TRACEログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	void trace(Object message, Throwable throwable);

	/**
	 * DEBUGログの出力有無を返します。
	 * @return DEBUGログの出力有無
	 */
	boolean isDebugEnabled();

	/**
	 * DEBUGログを出力します。
	 * @param message メッセージ
	 */
	void debug(Object message);

	/**
	 * DEBUGログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	void debug(Object message, Throwable throwable);

	/**
	 * INFOログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	boolean isInfoEnabled();

	/**
	 * INFOログを出力します。
	 * @param message メッセージ
	 */
	void info(Object message);

	/**
	 * INFOログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	void info(Object message, Throwable throwable);

	/**
	 * WARNログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	boolean isWarnEnabled();

	/**
	 * WARNログを出力します。
	 * @param message メッセージ
	 */
	void warn(Object message);

	/**
	 * WARNログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	void warn(Object message, Throwable throwable);

	/**
	 * ERRORログの出力有無を返します。
	 * @return ERRORログの出力有無
	 */
	boolean isErrorEnabled();

	/**
	 * ERRORログを出力します。
	 * @param message メッセージ
	 */
	void error(Object message);

	/**
	 * ERRORログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	void error(Object message, Throwable throwable);

	/**
	 * FATALログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	boolean isFatalEnabled();

	/**
	 * FATALログを出力します。
	 * @param message メッセージ
	 */
	void fatal(Object message);

	/**
	 * FATALログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	void fatal(Object message, Throwable throwable);

}
