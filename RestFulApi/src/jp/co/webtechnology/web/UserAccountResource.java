package jp.co.webtechnology.web;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import jp.co.webtechnology.fw.sql.DataAccessException;
import jp.co.webtechnology.fw.web.SystemException;
import jp.co.webtechnology.useraccount.UserAccountDto;
import jp.co.webtechnology.useraccount.UserAccountService;

/**
 * ユーザ情報に関するRestAPIクラスです。
 *
 * @author FSWeb S.Ogawa
 */
@Path("/UserAccount")
public class UserAccountResource {

	/**
	 * ユーザ情報を追加します。
	 * @param jaxbUserAccount JSON形式のユーザ情報
	 * @return 登録に成功した場合"success"を返します。
	 */
	@Path("/Insert")
	@POST
	@Consumes({MediaType.APPLICATION_JSON}) // requestのデータ形式
	@Produces(MediaType.TEXT_PLAIN) // responseのデータ形式
	public String createUser(JAXBElement<UserAccountDto> jaxbUserAccount) {
		System.out.println("createUser");
		UserAccountDto dto = jaxbUserAccount.getValue();
		System.out.println("userId:" + dto.getUserId());
		try {
			UserAccountService service = new UserAccountService();
			service.insert(jaxbUserAccount.getValue());

			return "success";
		} catch (DataAccessException e) {
			throw new SystemException("System error occured", e);
		}
	}

	/**
	 * ユーザ情報をすべてJSON形式で返します。
	 * @return ユーザ情報リストのJSON
	 */
	@Path("/SelectAll")
	@POST
	@Produces({MediaType.APPLICATION_JSON}) // responseのデータ形式
	public List<UserAccountDto> readAllUser() {
		try {
			UserAccountService service = new UserAccountService();
			List<UserAccountDto> userList = service.findAll();
			return userList;
		} catch (DataAccessException e) {
			throw new SystemException("System error occured", e);
		}
	}

	/**
	 * 渡されたユーザIDのユーザ情報を返します。
	 * @param userId ユーザID
	 * @return ユーザ情報のJSON
	 */
	@Path("/Select")
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED) // requestのデータ形式
	@Produces({MediaType.APPLICATION_JSON}) // responseのデータ形式
	public UserAccountDto readUser(@FormParam("userId") String userId) {
		try {
			UserAccountService service = new UserAccountService();
			UserAccountDto user = service.find(userId);
			return user;
		} catch (DataAccessException e) {
			throw new SystemException("System error occured", e);
		}
	}

//	/**
//	 * ユーザ情報を更新します。
//	 * @param jaxbUserAccount 更新するユーザ情報のJSON
//	 * @return 登録に成功した場合"success"を返します。
//	 */
//	@Path("/Update")
//	@POST
//	@Consumes(MediaType.APPLICATION_FORM_URLENCODED) // requestのデータ形式
//	@Produces(MediaType.TEXT_PLAIN) // responseのデータ形式
//	public String updateUser(@FormParam("userAccountDto") JAXBElement<UserAccountDto> jaxbUserAccount) {
//		try {
//			UserAccountService service = new UserAccountService();
//			service.update(jaxbUserAccount.getValue());
//
//			return "success";
//		} catch (DataAccessException e) {
//			throw new SystemException("System error occured", e);
//		}
//	}

	/**
	 * ユーザ情報を削除します。
	 * @param userId 削除するユーザのユーザID
	 * @return 削除に成功した場合"success"を返します。
	 */
	@Path("/Delete")
	@POST
	@Consumes(MediaType.TEXT_PLAIN) // requestのデータ形式
	@Produces(MediaType.TEXT_PLAIN) // responseのデータ形式
	public String deleteUser(@FormParam("userId") String userId) {
		try {
			UserAccountService service = new UserAccountService();
			service.delete(userId);
			return "success";
		} catch (DataAccessException e) {
			throw new SystemException("System error occured", e);
		}
	}

}
