package jp.co.webtechnology.fw.sql;

import java.sql.Connection;

import jp.co.webtechnology.fw.logger.Logger;
import jp.co.webtechnology.fw.logger.LoggerFactory;


/**
 * データアクセスオブジェクトの基底クラス。
 *
 * @author FSWeb H.Ikeda
 */
public abstract class Dao {

	/** Logger */
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	/** データベースコネクション */
	private final ConnectionWrapper conn;


	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param conn データベースコネクション
	 */
	protected Dao(Connection conn) {
		if (!(conn instanceof ConnectionWrapper)) {
			throw new IllegalArgumentException("conn is not ConnectionWrapper");
		}

		this.conn = (ConnectionWrapper) conn;
	}


	/**
	 * データベースコネクションを返します。
	 * @return データベースコネクション
	 */
	protected Connection getConnection() {
		return this.conn;
	}

	/**
	 * Loggerを返します。
	 * @return Logger
	 */
	protected Logger getLogger() {
		return this.logger;
	}


	/**
	 * SQLExecutorを生成します。
	 * @return SQLExecutor
	 */
	protected SqlExecutor newSqlExecutor() {
		return new SqlExecutor(this.conn, this.logger);
	}

}
