package jp.co.webtechnology.fw.sql;


/**
 * データベースアクセスエラーを表す例外クラス。
 *
 * @author WT H.Ikeda
 */
public class DataAccessException extends Exception {

	/** serialVersionUID */
	private static final long serialVersionUID = -6954188270519798859L;


	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param message メッセージ
	 */
	public DataAccessException(String message) {
		super(message);
	}

	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param cause 原因の例外
	 */
	public DataAccessException(Throwable cause) {
		super(cause);
	}

	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param message メッセージ
	 * @param cause 原因の例外
	 */
	public DataAccessException(String message, Throwable cause) {
		super(message, cause);
	}

}
