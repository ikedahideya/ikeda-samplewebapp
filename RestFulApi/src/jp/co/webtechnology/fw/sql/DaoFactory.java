package jp.co.webtechnology.fw.sql;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;

import jp.co.webtechnology.fw.logger.Logger;
import jp.co.webtechnology.fw.logger.LoggerFactory;


/**
 * データアクセスオブジェクトのインスタンス生成を制御するクラス。
 *
 * @author FSWeb H.Ikeda
 */
public final class DaoFactory {

	/** Logger */
	private static Logger logger = LoggerFactory.getLogger(DaoFactory.class);


	/**
	 * インスタンス化を禁止します。
	 */
	private DaoFactory() {
	}


	/**
	 * データアクセスオブジェクトのインスタンスを生成します。
	 * コネクションがモックモードの場合、モックモードのデータアクセスオブジェクトを生成します。
	 * コネクションのデータベース種別に合致する固有データアクセスオブジェクトが存在する場合、
	 * 固有オブジェクトを生成します。それ以外の場合は基本クラス名に対応するオブジェクトを生成します。
	 *
	 * @param baseDaoClass データアクセスオブジェクトの基本クラス名
	 * @param conn データベースコネクション
	 * @return データアクセスオブジェクト
	 */
	public static Dao newInstance(Class<?> baseDaoClass, ConnectionWrapper conn) {
		Class<?> daoClass = DaoFactory.resolveDaoClass(baseDaoClass, conn);

		// daoClass が baseDaoClass のサブクラスであることを確認
		if (!baseDaoClass.isAssignableFrom(daoClass)) {
			throw new DaoInstantiationException("Dao instantiation failed: " + daoClass.getName(),
					new ClassCastException("not assignable from " + baseDaoClass.getName()));
		}

		Dao dao = DaoFactory.constructDaoInstance(daoClass, conn);

		return dao;
	}


	/**
	 * 生成するデータアクセスオブジェクトのクラスを解決します。
	 *
	 * @param baseDaoClass データアクセスオブジェクトの基本クラス名
	 * @param conn データベースコネクション
	 * @return データベースコネクションのクラス
	 */
	private static Class<?> resolveDaoClass(Class<?> baseDaoClass, ConnectionWrapper conn) {
		String baseDaoClassName = baseDaoClass.getName();

		String personalDaoSuffix = conn.getPersonalDaoSuffix();
		if (personalDaoSuffix != null) {
			String extDaoClassName = baseDaoClassName + personalDaoSuffix;
			try {
				return Class.forName(extDaoClassName);
			} catch (ClassNotFoundException e) {
				if (DaoFactory.logger.isTraceEnabled()) {
					DaoFactory.logger.trace("Dao " + extDaoClassName + " not found.");
				}
			} catch (NoClassDefFoundError e) {
				// クラス名の大文字小文字が合わない場合
				throw new DaoInstantiationException(extDaoClassName + " not found.", e);
			}
		}

		String daoClassName = baseDaoClassName + conn.getDatabaseType().getDaoSuffix();
		try {
			return Class.forName(daoClassName);
		} catch (ClassNotFoundException e) {
			if (DaoFactory.logger.isTraceEnabled()) {
				DaoFactory.logger.trace("Dao " + daoClassName + " not found.");
			}
			return baseDaoClass;
		} catch (NoClassDefFoundError e) {
			// クラス名の大文字小文字が合わない場合
			throw new DaoInstantiationException(daoClassName + " not found.", e);
		}
	}

	/**
	 * データアクセスオブジェクトのインスタンスを生成します。
	 *
	 * @param daoClass データアクセスオブジェクトのクラス
	 * @param conn データベースコネクション
	 * @return データアクセスオブジェクトのインスタンス
	 */
	private static Dao constructDaoInstance(Class<?> daoClass, ConnectionWrapper conn) {
		Constructor<?>[] constructors = daoClass.getDeclaredConstructors();
		for (int i = 0; i < constructors.length; i++) {
			Constructor<?> constructor = constructors[i];
			Class<?>[] parameterTypes = constructor.getParameterTypes();
			if (parameterTypes.length == 1 && parameterTypes[0] == Connection.class) {
				try {
					Object dao = constructor.newInstance(new Object[] {conn});
					return (Dao) dao;
				} catch (InstantiationException e) {
					throw new DaoInstantiationException("Dao instantiation failed: " + daoClass.getName(), e);
				} catch (IllegalAccessException e) {
					throw new DaoInstantiationException("Dao instantiation failed: " + daoClass.getName(), e);
				} catch (IllegalArgumentException e) {
					throw new DaoInstantiationException("Dao instantiation failed: " + daoClass.getName(), e);
				} catch (InvocationTargetException e) {
					throw new DaoInstantiationException("Dao instantiation failed: " + daoClass.getName(), e);
				}
			}
		}

		throw new DaoInstantiationException("Dao constructor not found: " + daoClass.getName());
	}

}
