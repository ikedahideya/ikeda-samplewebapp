package jp.co.webtechnology.fw.sql;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * データベースのコネクションを管理するクラス。
 *
 * @author FSWeb S.Ogawa
 */
public class ConnectionManager {

	/** シングルトンインスタンス */
	private static ConnectionManager instance = null;
	/** DB設定ファイル名 */
	private static final String CONFIG_FILE_NAME = "connection_config.xml";

	/** ドライバクラス名 */
	private String driverClassName = null;
	/** 接続URL */
	private String url = null;
	/** ユーザID */
	private String userId = null;
	/** パスワード */
	private String password = null;

	/**
	 * コンストラクタです。DB設定を読み込みます。
	 * シングルトンクラスのため外部からのインスタンス生成を禁止します。
	 * @throws DataAccessException データベースアクセス中にエラーが発生した場合
	 */
	private ConnectionManager() throws DataAccessException {
		InputStream configInputStream = null;
		try {
			configInputStream = getClass().getClassLoader().getResourceAsStream(CONFIG_FILE_NAME);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(configInputStream);
			Element root = doc.getDocumentElement();

			NodeList nodeList = root.getElementsByTagName("property");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Element propElem = (Element) nodeList.item(i);
				String name = propElem.getAttribute("name");
				String value = propElem.getAttribute("value");

				if ("driverName".equals(name)) {
					this.driverClassName = value;
				} else if ("db_url".equals(name)) {
					this.url = value;
				} else if ("db_id".equals(name)) {
					this.userId = value;
				} else if ("db_password".equals(name)) {
					this.password = value;
				}
			}
		} catch (Exception e) {  // SAXException, IOException, ParserConfigurationException
			throw new DataAccessException("設定ファイル connection_config.xml の読み込み中にエラーが発生しました。", e);
		} finally {
			if (configInputStream != null) {
				try {
					configInputStream.close();
				} catch (IOException e) {
					throw new DataAccessException("設定ファイル connection_config.xml の読み込み中にエラーが発生しました。", e);
				}
			}
		}
	}

	/**
	 * シングルトンインスタンスを取得します。
	 * @return インスタンス
	 * @throws DataAccessException データベースアクセス中にエラーが発生した場合
	 */
	public static synchronized ConnectionManager getInstance() throws DataAccessException {
		if (instance == null) {
			instance = new ConnectionManager();
		}
		return instance;
	}

	/**
	 * 新規データベースコネクションを生成します。
	 * @return データベースコネクション
	 * @throws DataAccessException データベースアクセス中にエラーが発生した場合
	 */
	public Connection getConnection() throws DataAccessException {
		try {
			Class.forName(this.driverClassName);
			Connection conn = DriverManager.getConnection(this.url, this.userId, this.password);
			conn.setAutoCommit(false);
			ConnectionWrapper wrapper = new ConnectionWrapper(conn);
			return wrapper;
		} catch (ClassNotFoundException e) {
			throw new DataAccessException("データベースに接続できません。JDBCドライバのクラスパス設定を確認してください。", e);
		} catch (SQLException e) {
			throw new DataAccessException("データベースに接続できません。接続URL、ユーザID、パスワードを確認してください。", e);
		}
	}

}
