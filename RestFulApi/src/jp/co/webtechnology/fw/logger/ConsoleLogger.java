package jp.co.webtechnology.fw.logger;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * コンソールに出力するシンプルなLogger実装。
 *
 * @author WT H.Ikeda
 */
class ConsoleLogger implements Logger {


	/**
	 * Loggerインスタンスのファクトリクラス。
	 *
	 * @author WT H.Ikeda
	 */
	static class ConsoleLoggerInstanceFactory implements LoggerInstanceFactory {

		// 2012/11/15 WT W.Kubo Add Start
		/**
		 * 新規インスタンスを生成します。
		 * @param logConfig Logger設定ファイル名
		 */
		public ConsoleLoggerInstanceFactory(String logConfig) {
			// Logger設定ファイルは使用しない
		}
		// 2012/11/15 WT W.Kubo Add End

		/**
		 * Loggerインスタンスを生成します。
		 * @param name 名前
		 * @return Loggerインスタンス
		 */
		public Logger getLogger(String name) {
			return new ConsoleLogger(name);
		}

	}


	/** 日付のフォーマット */
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
	/** ログに出力する名前 */
	private final String shortName;


	/**
	 * 新規インスタンスを構築します。
	 * @param name 名前
	 */
	public ConsoleLogger(String name) {
		this.shortName = name.substring(name.lastIndexOf(".") + 1);
	}


	/**
	 * TRACEログを出力します。
	 * @param message メッセージ
	 */
	public void trace(Object message) {
		if (this.isTraceEnabled()) {
			this.log(LogLevel.TRACE, message);
		}
	}

	/**
	 * TRACEログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void trace(Object message, Throwable throwable) {
		if (this.isTraceEnabled()) {
			this.log(LogLevel.TRACE, message, throwable);
		}
	}

	/**
	 * DEBUGログを出力します。
	 * @param message メッセージ
	 */
	public void debug(Object message) {
		if (this.isDebugEnabled()) {
			this.log(LogLevel.DEBUG, message);
		}
	}

	/**
	 * DEBUGログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void debug(Object message, Throwable throwable) {
		if (this.isDebugEnabled()) {
			this.log(LogLevel.DEBUG, message, throwable);
		}
	}

	/**
	 * INFOログを出力します。
	 * @param message メッセージ
	 */
	public void info(Object message) {
		if (this.isInfoEnabled()) {
			this.log(LogLevel.INFO, message);
		}
	}

	/**
	 * INFOログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void info(Object message, Throwable throwable) {
		if (this.isInfoEnabled()) {
			this.log(LogLevel.INFO, message, throwable);
		}
	}

	/**
	 * WARNログを出力します。
	 * @param message メッセージ
	 */
	public void warn(Object message) {
		if (this.isWarnEnabled()) {
			this.log(LogLevel.WARN, message);
		}
	}

	/**
	 * WARNログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void warn(Object message, Throwable throwable) {
		if (this.isWarnEnabled()) {
			this.log(LogLevel.WARN, message, throwable);
		}
	}

	/**
	 * ERRORログを出力します。
	 * @param message メッセージ
	 */
	public void error(Object message) {
		if (this.isErrorEnabled()) {
			this.log(LogLevel.ERROR, message);
		}
	}

	/**
	 * ERRORログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void error(Object message, Throwable throwable) {
		if (this.isErrorEnabled()) {
			this.log(LogLevel.ERROR, message, throwable);
		}
	}

	/**
	 * FATALログを出力します。
	 * @param message メッセージ
	 */
	public void fatal(Object message) {
		if (this.isFatalEnabled()) {
			this.log(LogLevel.FATAL, message);
		}
	}

	/**
	 * FATALログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void fatal(Object message, Throwable throwable) {
		if (this.isFatalEnabled()) {
			this.log(LogLevel.FATAL, message, throwable);
		}
	}

	/**
	 * TRACEログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	public boolean isTraceEnabled() {
		return LoggerManager.getInstance().getLogLevel().isTraceEnabled();
	}

	/**
	 * DEBUGログの出力有無を返します。
	 * @return DEBUGログの出力有無
	 */
	public boolean isDebugEnabled() {
		return LoggerManager.getInstance().getLogLevel().isDebugEnabled();
	}

	/**
	 * INFOログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	public boolean isInfoEnabled() {
		return LoggerManager.getInstance().getLogLevel().isInfoEnabled();
	}

	/**
	 * WARNログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	public boolean isWarnEnabled() {
		return LoggerManager.getInstance().getLogLevel().isWarnEnabled();
	}

	/**
	 * ERRORログの出力有無を返します。
	 * @return ERRORログの出力有無
	 */
	public boolean isErrorEnabled() {
		return LoggerManager.getInstance().getLogLevel().isErrorEnabled();
	}

	/**
	 * FATALログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	public boolean isFatalEnabled() {
		return LoggerManager.getInstance().getLogLevel().isFatalEnabled();
	}


	/**
	 * 実際にログを出力します。
	 *
	 * @param level ログレベル
	 * @param message メッセージ
	 */
	private void log(LogLevel level, Object message) {
		StringBuffer buf = new StringBuffer();
		synchronized (this) {
			buf.append(dateFormat.format(new Date()));
		}
		buf.append(" [").append(this.getCurrentThreadName()).append("] ");
		buf.append(level.name()).append(" ");
		buf.append(this.shortName).append(" - ");
		buf.append(message);

		System.out.println(buf.toString());
	}

	/**
	 * 実際にログを出力します。
	 *
	 * @param level ログレベル
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	private void log(LogLevel level, Object message, Throwable throwable) {
		this.log(level, message);
		if (throwable != null) {
			throwable.printStackTrace(System.out);
		}
	}

	/**
	 * 現在のスレッド名を返します。
	 * @return スレッド名
	 */
	private String getCurrentThreadName() {
		Thread currentThread = Thread.currentThread();
		if (currentThread != null) {
			return currentThread.getName();
		} else {
			// たぶん来ることはないと思う
			return "Unknown";
		}
	}

}
