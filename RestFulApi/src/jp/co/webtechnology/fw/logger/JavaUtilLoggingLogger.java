package jp.co.webtechnology.fw.logger;

import java.util.logging.Level;


/**
 * java.util.logging に対応したLogger実装。
 *
 * @author WT H.Ikeda
 */
class JavaUtilLoggingLogger implements Logger {


	/**
	 * Loggerインスタンスのファクトリクラス。
	 *
	 * @author WT H.Ikeda
	 */
	static class JavaUtilLoggingLoggerFactory implements LoggerInstanceFactory {

		// 2012/11/15 WT W.Kubo Add Start
		/**
		 * 新規インスタンスを生成します。
		 * @param logConfig Logger設定ファイル名
		 */
		public JavaUtilLoggingLoggerFactory(String logConfig) {
			// Logger設定ファイルは使用しない
		}
		// 2012/11/15 WT W.Kubo Add End

		/**
		 * Loggerインスタンスを生成します。
		 * @param name 名前
		 * @return Loggerインスタンス
		 */
		public Logger getLogger(String name) {
			return new JavaUtilLoggingLogger(name);
		}

	}


	/** ログのメソッド名の部分に出力する文字列 */
	private static final String METHOD_NAME = null;

	/** 名前 */
	private final String name;
	/** java.util.logging のLogger */
	private final java.util.logging.Logger logger;

	/**
	 * 新規インスタンスを構築します。
	 * @param name 名前
	 */
	JavaUtilLoggingLogger(String name) {
		this.name = name;
		this.logger = java.util.logging.Logger.getLogger(name);
	}


	/**
	 * TRACEログを出力します。
	 * @param message メッセージ
	 */
	public void trace(Object message) {
		if (this.isTraceEnabled()) {
			this.logger.logp(Level.FINEST, name, METHOD_NAME, this.messageToString(message));
		}
	}

	/**
	 * TRACEログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void trace(Object message, Throwable throwable) {
		if (this.isTraceEnabled()) {
			this.logger.logp(Level.FINEST, name, METHOD_NAME, this.messageToString(message), throwable);
		}
	}

	/**
	 * DEBUGログを出力します。
	 * @param message メッセージ
	 */
	public void debug(Object message) {
		if (this.isDebugEnabled()) {
			this.logger.logp(Level.FINE, name, METHOD_NAME, this.messageToString(message));
		}
	}

	/**
	 * DEBUGログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void debug(Object message, Throwable throwable) {
		if (this.isDebugEnabled()) {
			this.logger.logp(Level.FINE, name, METHOD_NAME, this.messageToString(message), throwable);
		}
	}

	/**
	 * INFOログを出力します。
	 * @param message メッセージ
	 */
	public void info(Object message) {
		if (this.isInfoEnabled()) {
			this.logger.logp(Level.INFO, name, METHOD_NAME, this.messageToString(message));
		}
	}

	/**
	 * INFOログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void info(Object message, Throwable throwable) {
		if (this.isInfoEnabled()) {
			this.logger.logp(Level.INFO, name, METHOD_NAME, this.messageToString(message), throwable);
		}
	}

	/**
	 * WARNログを出力します。
	 * @param message メッセージ
	 */
	public void warn(Object message) {
		if (this.isWarnEnabled()) {
			this.logger.logp(Level.WARNING, name, METHOD_NAME, this.messageToString(message));
		}
	}

	/**
	 * WARNログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void warn(Object message, Throwable throwable) {
		if (this.isWarnEnabled()) {
			this.logger.logp(Level.WARNING, name, METHOD_NAME, this.messageToString(message), throwable);
		}
	}

	/**
	 * ERRORログを出力します。
	 * @param message メッセージ
	 */
	public void error(Object message) {
		if (this.isErrorEnabled()) {
			this.logger.logp(Level.SEVERE, name, METHOD_NAME, this.messageToString(message));
		}
	}

	/**
	 * ERRORログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void error(Object message, Throwable throwable) {
		if (this.isErrorEnabled()) {
			this.logger.logp(Level.SEVERE, name, METHOD_NAME, this.messageToString(message), throwable);
		}
	}

	/**
	 * FATALログを出力します。
	 * @param message メッセージ
	 */
	public void fatal(Object message) {
		if (this.isFatalEnabled()) {
			this.logger.logp(Level.SEVERE, name, METHOD_NAME, this.messageToString(message));
		}
	}

	/**
	 * FATALログを出力します。
	 * @param message メッセージ
	 * @param throwable 例外
	 */
	public void fatal(Object message, Throwable throwable) {
		if (this.isFatalEnabled()) {
			this.logger.logp(Level.SEVERE, name, METHOD_NAME, this.messageToString(message), throwable);
		}
	}

	/**
	 * TRACEログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	public boolean isTraceEnabled() {
		return LoggerManager.getInstance().getLogLevel().isTraceEnabled() && this.logger.isLoggable(Level.FINEST);
	}

	/**
	 * DEBUGログの出力有無を返します。
	 * @return DEBUGログの出力有無
	 */
	public boolean isDebugEnabled() {
		return LoggerManager.getInstance().getLogLevel().isDebugEnabled() && this.logger.isLoggable(Level.FINE);
	}

	/**
	 * INFOログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	public boolean isInfoEnabled() {
		return LoggerManager.getInstance().getLogLevel().isInfoEnabled() && this.logger.isLoggable(Level.INFO);
	}

	/**
	 * WARNログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	public boolean isWarnEnabled() {
		return LoggerManager.getInstance().getLogLevel().isWarnEnabled() && this.logger.isLoggable(Level.WARNING);
	}

	/**
	 * ERRORログの出力有無を返します。
	 * @return ERRORログの出力有無
	 */
	public boolean isErrorEnabled() {
		return LoggerManager.getInstance().getLogLevel().isErrorEnabled() && this.logger.isLoggable(Level.SEVERE);
	}

	/**
	 * FATALログの出力有無を返します。
	 * @return INFOログの出力有無
	 */
	public boolean isFatalEnabled() {
		return LoggerManager.getInstance().getLogLevel().isFatalEnabled() && this.logger.isLoggable(Level.SEVERE);
	}


	/**
	 * 引数 message をString型に変換します。
	 * null の場合に "null" を返します。
	 * それ以外は message の toString() の結果を返します。
	 *
	 * @param message メッセージ(Object型)
	 * @return メッセージ(String型)
	 */
	private String messageToString(Object message) {
		if (message == null) {
			return "null";
		} else {
			return message.toString();
		}
	}

}
