package jp.co.webtechnology.useraccount;

import java.sql.Connection;
import java.sql.SQLException;

import jp.co.webtechnology.fw.sql.DataAccessException;
import jp.co.webtechnology.fw.sql.SqlExecutor;


//DB固有のSQL構文がない場合、UserAccountDaoクラスを抽象クラスにせず、本クラスを削除してUserAccountDaoクラスのみで運用できます。
/**
 * PostgreSQL/Symfoware用のUSERACCOUNTテーブルのDaoクラスです。
 *
 * @author FSWeb S.Ogawa
 */
public class UserAccountDaoPostgreSQLImpl extends UserAccountDao {

	/**
	 * コンストラクタです。
	 * @param conn DBConnection
	 */
	public UserAccountDaoPostgreSQLImpl(Connection conn) {
		super(conn);
	}

	// DB固有のSQL文がある場合の実装例です。今回は使用していません。
	/**
	 * UserAccountテーブルをロックします。
	 * @throws DataAccessException データベースアクセス中にエラーが発生した場合
	 */
	@Override
	public void lockTableUserAccount() throws DataAccessException {
		SqlExecutor executor = super.newSqlExecutor();
		try {
			executor.append("LOCK TABLE USERACCOUNT IN ACCESS EXCLUSIVE MODE");
			executor.executeUpdate();
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			executor.closePreparedStatement();
		}
	}

}
