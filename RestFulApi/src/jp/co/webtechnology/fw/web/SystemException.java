package jp.co.webtechnology.fw.web;

/**
 * システムエラーを表す例外クラス。
 *
 * @author FSWeb H.Ikeda
 */
public class SystemException extends RuntimeException {

	/** serialVersionUID */
	private static final long serialVersionUID = 4339696208233667999L;


	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param message エラーメッセージ
	 */
	public SystemException(String message) {
		super(message);
	}

	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param message エラーメッセージ
	 * @param cause 原因の例外
	 */
	public SystemException(String message, Throwable cause) {
		super(message, cause);
	}

}
