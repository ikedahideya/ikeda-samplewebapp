package jp.co.webtechnology.useraccount;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import jp.co.webtechnology.fw.sql.ConnectionManager;
import jp.co.webtechnology.fw.sql.ConnectionWrapper;
import jp.co.webtechnology.fw.sql.DaoFactory;
import jp.co.webtechnology.fw.sql.DataAccessException;


/**
 * ユーザー情報に対するServiceクラス
 *
 * @author FSWeb S.Ogawa
 */
public class UserAccountService {

	/**
	 * ユーザ情報を全件検索します。
	 * @return ユーザ情報リスト
	 * @throws DataAccessException データベースアクセス時に例外が発生した場合
	 */
	public List<UserAccountDto> findAll() throws DataAccessException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			UserAccountDao dao = (UserAccountDao) DaoFactory.newInstance(UserAccountDao.class, (ConnectionWrapper)connection);
			List<UserAccountDto> resultList = dao.findAll();

			connection.commit();
			return resultList;
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					throw new DataAccessException(e);
				}
			}
		}
	}

	/**
	 * 主キーからユーザ情報を検索します。
	 * @param userId ユーザID
	 * @throws DataAccessException データベースアクセス時に例外が発生した場合
	 */
	public UserAccountDto find(String userId) throws DataAccessException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			UserAccountDao dao = (UserAccountDao) DaoFactory.newInstance(UserAccountDao.class, (ConnectionWrapper)connection);
			UserAccountDto result = dao.findByPK(userId);

			connection.commit();
			return result;
		} catch (SQLException e) {
			throw new DataAccessException(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					throw new DataAccessException(e);
				}
			}
		}
	}

	/**
	 * ユーザ情報を追加します。
	 * @param dto 追加するユーザ情報
	 * @throws DataAccessException データベースアクセス時に例外が発生した場合
	 */
	public void insert(UserAccountDto dto) throws DataAccessException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			connection = ConnectionManager.getInstance().getConnection();
			UserAccountDao dao = (UserAccountDao) DaoFactory.newInstance(UserAccountDao.class, (ConnectionWrapper)connection);
			dao.insert(dto);

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				throw new DataAccessException(e);
			}
			throw new DataAccessException(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					throw new DataAccessException(e);
				}
			}
		}
	}

	/**
	 * ユーザ情報を更新します。
	 * 更新するUSERIDが存在しなかった場合、ユーザ情報を追加します。
	 * @param dto 更新するユーザ情報
	 * @throws DataAccessException データベースアクセス時に例外が発生した場合
	 */
	public void update(UserAccountDto dto) throws DataAccessException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			UserAccountDao dao = (UserAccountDao) DaoFactory.newInstance(UserAccountDao.class, (ConnectionWrapper)connection);
			if (dao.update(dto) <= 0) {
				dao.insert(dto);
			}

			connection.commit();
		} catch (SQLException e) {
			if (connection != null) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
					throw new DataAccessException(e);
				}
			}
			throw new DataAccessException(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					throw new DataAccessException(e);
				}
			}
		}
	}

	/**
	 * ユーザ情報を削除します。
	 * @param userId 削除するユーザ情報のユーザID
	 * @throws DataAccessException データベースアクセス時に例外が発生した場合
	 */
	public void delete(String userId) throws DataAccessException {
		Connection connection = null;
		try {
			connection = ConnectionManager.getInstance().getConnection();
			UserAccountDao dao = (UserAccountDao) DaoFactory.newInstance(UserAccountDao.class, (ConnectionWrapper)connection);
			dao.delete(userId);

			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				throw new DataAccessException(e);
			}
			throw new DataAccessException(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					throw new DataAccessException(e);
				}
			}
		}
	}

}
