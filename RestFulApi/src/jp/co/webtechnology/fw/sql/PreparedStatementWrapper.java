package jp.co.webtechnology.fw.sql;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

import jp.co.webtechnology.fw.logger.Logger;
import jp.co.webtechnology.fw.logger.LoggerFactory;


/**
 * PreparedStatementのラッパークラス。
 *
 * @author FSWeb H.Ikeda
 */
class PreparedStatementWrapper implements PreparedStatement {

	/** Logger */
	private static Logger logger = LoggerFactory.getLogger(PreparedStatementWrapper.class);

	/** PreparedStatement */
	private final PreparedStatement stmt;
	/** データベースの種類 */
	private final DatabaseType databaseType;
	/** リソース解放状態 */
	private boolean isClosed = false;


	/**
	 * 新規インスタンスを構築します。
	 *
	 * @param conn データベースコネクション
	 * @param sql SQL
	 * @param databaseType データベースの種類
	 * @throws SQLException インスタンスの構築に失敗した場合
	 */
	public PreparedStatementWrapper(Connection conn, String sql, DatabaseType databaseType) throws SQLException {
		this.stmt = conn.prepareStatement(sql);
		this.databaseType = databaseType;

		// PostgreSQLはフェッチサイズを明示しないと検索結果を全件メモリ展開する
		if (this.databaseType == DatabaseType.POSTGRESQL) {
			this.stmt.setFetchSize(100);
		}

		if (logger.isTraceEnabled()) {
			// 共通部品外の行をピンポイント出力
			String openLocation = "?";
			StackTraceElement[] stacktraces = new Throwable().getStackTrace();
			for (int i = 0; i < stacktraces.length; i++) {
				String clazz = stacktraces[i].getClassName();
				if (clazz.startsWith("java.lang.") ||
						clazz.startsWith("jp.co.webtechnology.dp.sql.")) {
					continue;
				}

				openLocation = stacktraces[i].toString();
				break;
			}
			logger.trace("open[" + this.stmt.getClass().getName() + "@" + this.stmt.hashCode() + ",conn=@" + conn.hashCode() + "] at " + openLocation);
			logger.trace(sql);
		}
	}


	/** {@inheritDoc} */
	public void setInt(int parameterIndex, int x) throws SQLException {
		logger.trace("[" + parameterIndex + "]=" + x);
		this.stmt.setInt(parameterIndex, x);
	}

	/** {@inheritDoc} */
	public void setLong(int parameterIndex, long x) throws SQLException {
		logger.trace("[" + parameterIndex + "]=" + x);
		this.stmt.setLong(parameterIndex, x);
	}

	/** {@inheritDoc} */
	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		logger.trace("[" + parameterIndex + "]=" + x);
		this.stmt.setBigDecimal(parameterIndex, x);
	}

	/** {@inheritDoc} */
	public void setString(int parameterIndex, String x) throws SQLException {
		if (x == null) {
			logger.trace("[" + parameterIndex + "]=null");
		} else {
			logger.trace("[" + parameterIndex + "]=" + x);
		}
		this.stmt.setString(parameterIndex, x);
	}

	/** {@inheritDoc} */
	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		if (x == null) {
			logger.trace("[" + parameterIndex + "]=null");
		} else {
			logger.trace("[" + parameterIndex + "]=*(" + x.length + "bytes)");
		}
		this.stmt.setBytes(parameterIndex, x);
	}

	/** {@inheritDoc} */
	public void setBinaryStream(int parameterIndex, InputStream x, int length)
			throws SQLException {

		logger.trace("[" + parameterIndex + "]=*(binary)");
		this.stmt.setBinaryStream(parameterIndex, x, length);
	}


	/** {@inheritDoc} */
	public void clearParameters() throws SQLException {
		this.stmt.clearParameters();
	}

	/** {@inheritDoc} */
	public ResultSet executeQuery() throws SQLException {
		ResultSet rs = this.stmt.executeQuery();
		return new ResultSetWrapper(this.stmt, rs, this.databaseType);
	}

	/** {@inheritDoc} */
	public int executeUpdate() throws SQLException {
		return this.stmt.executeUpdate();
	}

	/** {@inheritDoc} */
	public void close() throws SQLException {
		this.stmt.close();
		this.isClosed = true;
	}

	/**
	 * ファイナライズ処理を実行します。
	 * リソースの解放漏れを検出して警告ログを出力します。
	 *
	 * @throws Throwable 処理中にエラーが発生した場合
	 */
	protected void finalize() throws Throwable {
		try {
			if (!this.isClosed) {
				logger.debug("LEAK![@" + this.stmt.hashCode() + "]");
				this.close();
			}
		} finally {
			super.finalize();
		}
	}


	// 以降、使用想定外

	/** {@inheritDoc} */
	@Override
	public ResultSet executeQuery(String sql) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int executeUpdate(String sql) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getMaxFieldSize() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setMaxFieldSize(int max) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getMaxRows() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setMaxRows(int max) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setEscapeProcessing(boolean enable) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getQueryTimeout() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setQueryTimeout(int seconds) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void cancel() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void clearWarnings() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setCursorName(String name) throws SQLException {
		throw new UnsupportedOperationException();

	}


	/** {@inheritDoc} */
	@Override
	public boolean execute(String sql) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public ResultSet getResultSet() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getUpdateCount() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean getMoreResults() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setFetchDirection(int direction) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getFetchDirection() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setFetchSize(int rows) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getFetchSize() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getResultSetConcurrency() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getResultSetType() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void addBatch(String sql) throws SQLException {
		throw new UnsupportedOperationException();

	}


	/** {@inheritDoc} */
	@Override
	public void clearBatch() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int[] executeBatch() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public Connection getConnection() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean getMoreResults(int current) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int executeUpdate(String sql, int autoGeneratedKeys)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int executeUpdate(String sql, int[] columnIndexes)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int executeUpdate(String sql, String[] columnNames)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean execute(String sql, int autoGeneratedKeys)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean execute(String sql, String[] columnNames)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public int getResultSetHoldability() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isClosed() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setPoolable(boolean poolable) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isPoolable() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void closeOnCompletion() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setByte(int parameterIndex, byte x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setShort(int parameterIndex, short x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setFloat(int parameterIndex, float x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setDouble(int parameterIndex, double x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setDate(int parameterIndex, Date x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setTime(int parameterIndex, Time x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setTimestamp(int parameterIndex, Timestamp x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setUnicodeStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setObject(int parameterIndex, Object x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public boolean execute() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void addBatch() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setCharacterStream(int parameterIndex, Reader reader, int length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setRef(int parameterIndex, Ref x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setClob(int parameterIndex, Clob x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setArray(int parameterIndex, Array x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setDate(int parameterIndex, Date x, Calendar cal)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setTime(int parameterIndex, Time x, Calendar cal)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setNull(int parameterIndex, int sqlType, String typeName)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setURL(int parameterIndex, URL x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setNString(int parameterIndex, String value)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setNCharacterStream(int parameterIndex, Reader value,
			long length) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setBlob(int parameterIndex, InputStream inputStream, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setNClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setSQLXML(int parameterIndex, SQLXML xmlObject)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setObject(int parameterIndex, Object x, int targetSqlType,
			int scaleOrLength) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setAsciiStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setBinaryStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setCharacterStream(int parameterIndex, Reader reader,
			long length) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setAsciiStream(int parameterIndex, InputStream x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setBinaryStream(int parameterIndex, InputStream x)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setCharacterStream(int parameterIndex, Reader reader)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setNCharacterStream(int parameterIndex, Reader value)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setBlob(int parameterIndex, InputStream inputStream)
			throws SQLException {
		throw new UnsupportedOperationException();
	}


	/** {@inheritDoc} */
	@Override
	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		throw new UnsupportedOperationException();
	}

}
