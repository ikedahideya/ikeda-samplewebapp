package jp.co.webtechnology.fw.sql;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.webtechnology.fw.logger.Logger;
import jp.co.webtechnology.fw.logger.LoggerFactory;


/**
 * SQL実行を効率的に記述できるヘルパークラス。
 *
 * <p>
 * このクラスと、関連する {@link ConnectionWrapper} 系は、
 * 各種データベースの差異を吸収するための下記の仕組みを実装しています。
 * <ul>
 * <li>SUBSTR関数の文法の違いを吸収(SQLServerのみSUBSTRING)</li>
 * <li>""の解釈の違いを吸収(Oracleのみ""をNULLとする)
 * <ul>
 * <li>Resultset#getStringは""をnullと解釈することで統一</li>
 * <li>nullまたは""を判定する式を作成するメソッドを提供</li>
 * </ul>
 * <li>PostgreSQLの場合にPreparedStatementにフェッチサイズを自動設定
 * (明示的に設定しないと検索結果を全件メモリ展開する)</li>
 * </ul>
 * </p>
 *
 * @author FSWeb H.Ikeda
 */
public class SqlExecutor {

	private static class BinaryStream {
		/** InputStream */
		private InputStream inputStream;

		/** Length */
		private int length;

		public BinaryStream(InputStream inputStream, int length) {
			this.inputStream = inputStream;
			this.length = length;
		}

		/**
		 * InputStreamを取得します。
		 * @return inputStream InputStreamオブジェクト
		 */
		public InputStream getInputStream() {
			return inputStream;
		}

		/**
		 * InputStreamの長さを取得します。
		 * @return length InputStreamの長さ
		 */
		public int getLength() {
			return length;
		}
	}

	/** バインド変数のString型のnullを表す内部定数 */
	private static final Object STRING_NULL = new Object();
	/** バインド変数のbyte[]型のnullを表す内部定数 */
	private static final Object BYTES_NULL = new Object();


	/** データベースコネクション */
	private final ConnectionWrapper conn;
	/** データベースの種類 */
	private final DatabaseType databaseType;
	/** Logger */
	private final Logger logger;

	/** PreparedStatement */
	private PreparedStatement stmt;
	/** ResultSet */
	private ResultSet rs;

	/** SQL */
	private StringBuffer sqlBuffer = new StringBuffer();
	/** SQL確定フラグ */
	private boolean sqlFixed = false;

	/** バインド変数の値リスト */
	private List<Object> bindParams = new ArrayList<Object>();


	/**
	 * 新規インスタンスを生成します。
	 *
	 * @param conn データベースコネクション
	 * @param logger Logger
	 */
	public SqlExecutor(Connection conn, Logger logger) {
		if (!(conn instanceof ConnectionWrapper)) {
			throw new IllegalArgumentException("conn is not ConnectionWrapper");
		}

		this.conn = (ConnectionWrapper) conn;
		this.databaseType = this.conn.getDatabaseType();
		this.logger = logger;
	}

	/**
	 * SQL文字列を構築します。
	 *
	 * @param sqlStr SQL文字列
	 */
	public void append(String sqlStr) {
		if (this.sqlFixed) {
			throw new IllegalStateException("SQL実行後には変更できません。");
		}

		this.sqlBuffer.append(sqlStr);
	}

	/**
	 * SUBSTR句の文字列を構築します。
	 *
	 * @param columnStr 対象カラム名
	 * @param beginIndex 開始インデックス
	 * @param length 長さ
	 */
	public void appendSubstr(String columnStr, int beginIndex, int length) {
		if (this.sqlFixed) {
			throw new IllegalStateException("SQL実行後には変更できません。");
		}

		if (this.databaseType == DatabaseType.SQLSERVER) {
			this.append("SUBSTRING(");
			this.append(columnStr);
			this.append(",");
			this.append(String.valueOf(beginIndex));
			this.append(",");
			this.append(String.valueOf(length));
			this.append(")");
		} else if (this.databaseType == DatabaseType.ORACLE ||
				this.databaseType == DatabaseType.POSTGRESQL ||
				this.databaseType == DatabaseType.UNKNOWN) {

			this.append("SUBSTR(");
			this.append(columnStr);
			this.append(",");
			this.append(String.valueOf(beginIndex));
			this.append(",");
			this.append(String.valueOf(length));
			this.append(")");
		} else {
			throw new AssertionError();
		}
	}

	/**
	 * IN句の文字列を構築し、値をバインドします。
	 *
	 * @param columnName カラム名
	 * @param values 値の配列
	 */
	public void appendBindIn(String columnName, String[] values) {
		this.append(columnName);
		this.append(" IN (");
		for (int i = 0; i < values.length; i++) {
			this.append("?");
			this.setString(values[i]);
			if (i < values.length - 1) {
				this.append(",");
			}
		}
		this.append(")");
	}

	/**
	 * 指定したカラムが NULL または "" であることを判定するSQL条件式を返します。
	 *
	 * @param columnName カラム名
	 */
	public void appendIsNullOrEmpty(String columnName) {
		if (this.databaseType == DatabaseType.ORACLE) {
			this.append(columnName);
			this.append(" IS NULL");
		} else {
			this.append("(");
			this.append(columnName);
			this.append(" IS NULL");
			this.append(" OR ");
			this.append(columnName);
			this.append("=''");
			this.append(")");
		}
	}

	/**
	 * 指定したカラムが NULL でも "" でもないことを判定するSQL条件式を返します。
	 *
	 * @param columnName カラム名
	 */
	public void appendIsNotNullAndNotEmpty(String columnName) {
		if (this.databaseType == DatabaseType.ORACLE) {
			this.append(columnName);
			this.append(" IS NOT NULL");
		} else {
			this.append("(");
			this.append(columnName);
			this.append(" IS NOT NULL");
			this.append(" AND ");
			this.append(columnName);
			this.append("<>''");
			this.append(")");
		}
	}

	/**
	 * String型のバインド変数を設定します。
	 *
	 * @param value 値
	 */
	public void setString(String value) {
		if (value == null) {
			this.bindParams.add(STRING_NULL);
		} else {
			this.bindParams.add(value);
		}
	}

	/**
	 * int型のバインド変数を設定します。
	 *
	 * @param value 値
	 */
	public void setInt(int value) {
		// 桁数チェックはしない。
		// trimするのも変な話なのでSQLExceptionになってしまえばいい。

		this.bindParams.add(new Integer(value));
	}

	/**
	 * long型のバインド変数を設定します。
	 *
	 * @param value 値
	 */
	public void setLong(long value) {
		// 桁数チェックはしない。
		// trimするのも変な話なのでSQLExceptionになってしまえばいい。

		this.bindParams.add(new Long(value));
	}

	/**
	 * バイナリ型のバインド変数を設定します。
	 *
	 * @param value 値
	 */
	public void setBytes(byte[] value) {
		if (value == null) {
			this.bindParams.add(BYTES_NULL);
		} else {
			this.bindParams.add(value);
		}
	}

	/**
	 * InputStream型のバインド変数を設定します。
	 *
	 * @param value 値
	 * @param length 長さ
	 */
	public void setBinaryStream(InputStream value, int length) {
		BinaryStream bs = new BinaryStream(value, length);

		if (value == null) {
			this.bindParams.add(BYTES_NULL);
		} else {
			this.bindParams.add(bs);
		}
	}



	/**
	 * 検索系SQLを実行します。
	 *
	 * @return ResultSet
	 * @throws SQLException データアクセス中にエラーが発生した場合
	 */
	public ResultSet executeQuery() throws SQLException {
		if (!this.sqlFixed) {
			this.sqlFixed = true;
		}

		try {
			this.closeResultSet();

			if (this.stmt == null) {
				this.stmt = this.createPreparedStatement();
			}
			this.bindParams(this.stmt);
			this.rs = this.stmt.executeQuery();

			return rs;
		} finally {
			this.bindParams = new ArrayList<Object>();
			if (this.stmt != null) {
				this.stmt.clearParameters();
			}
		}
	}

	/**
	 * 更新系SQLを実行します。
	 *
	 * @return 更新件数
	 * @throws SQLException データアクセス中にエラーが発生した場合
	 */
	public int executeUpdate() throws SQLException {
		if (!this.sqlFixed) {
			this.sqlFixed = true;
		}

		try {
			if (this.stmt == null) {
				this.stmt = this.createPreparedStatement();
			}
			this.bindParams(this.stmt);
			int result = this.stmt.executeUpdate();

			return result;
		} finally {
			this.bindParams = new ArrayList<Object>();
			this.stmt.clearParameters();
		}
	}


	/**
	 * PreparedStatementを生成します。
	 *
	 * @return PreparedStatement
	 * @throws SQLException データアクセス中にエラーが発生した場合
	 */
	private PreparedStatement createPreparedStatement() throws SQLException {
		String sql = this.sqlBuffer.toString();
		logger.debug("SQL=" + sql);

		PreparedStatement stmt = this.conn.prepareStatement(sql);
		return stmt;
	}

	/**
	 * PreparedStatementにバインド変数を設定します。
	 *
	 * @param stmt PreparedStatement
	 * @throws SQLException データアクセス中にエラーが発生した場合
	 */
	private void bindParams(PreparedStatement stmt) throws SQLException {
		for (int i = 0; i < this.bindParams.size(); i++) {
			Object bindValue = this.bindParams.get(i);

			if (bindValue == STRING_NULL) {
				stmt.setString(i + 1, null);
			} else if (bindValue == BYTES_NULL) {
				stmt.setBytes(i + 1, null);
			} else if (bindValue instanceof String) {
				stmt.setString(i + 1, (String) bindValue);
			} else if (bindValue instanceof Integer) {
				stmt.setInt(i + 1, ((Integer) bindValue).intValue());
			} else if (bindValue instanceof Long) {
				stmt.setLong(i + 1, ((Long) bindValue).longValue());
			} else if (bindValue instanceof byte[]) {
				stmt.setBytes(i + 1, (byte[]) bindValue);
			} else if (bindValue instanceof BinaryStream) {
				stmt.setBinaryStream(i + 1, ((BinaryStream) bindValue).getInputStream(), ((BinaryStream) bindValue).getLength());
			}
		}
	}


	/**
	 * ResultSetをクローズします。
	 */
	public void closeResultSet() {
		if (this.rs != null) {
			try {
				this.rs.close();
			} catch (SQLException e) {
				Logger logger = LoggerFactory.getLogger(SqlExecutor.class);
				logger.debug("リソースの開放に失敗しました。", e);
			}
		}

		this.rs = null;
	}

	/**
	 * PreparedStatementをクローズします。
	 */
	public void closePreparedStatement() {
		if (this.stmt != null) {
			try {
				this.stmt.close();
			} catch (SQLException e) {
				Logger logger = LoggerFactory.getLogger(SqlExecutor.class);
				logger.debug("リソースの開放に失敗しました。", e);
			}
		}

		this.stmt = null;
	}

}
