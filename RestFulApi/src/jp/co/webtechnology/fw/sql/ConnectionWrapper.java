package jp.co.webtechnology.fw.sql;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import jp.co.webtechnology.fw.logger.Logger;
import jp.co.webtechnology.fw.logger.LoggerFactory;


/**
 * Connectionのラッパークラス。
 *
 * @author FSWeb H.Ikeda
 */
public class ConnectionWrapper implements Connection {

	/** Logger */
	private static Logger logger = LoggerFactory.getLogger(ConnectionWrapper.class);

	/** データベースコネクション */
	private final Connection conn;
	/** データベースの種類 */
	private final DatabaseType databaseType;
	/** 個別Dao実装クラスの接尾辞 */
	private String personalDaoSuffix;
	/** リソース解放状態 */
	private boolean isClosed = false;


	/**
	 * 新規インスタンスを構築します。
	 * @param conn データベースコネクション
	 */
	public ConnectionWrapper(Connection conn) {
		this.conn = conn;
		this.databaseType = DatabaseType.resolveDatabaseType(conn);

		if (logger.isTraceEnabled()) {
			// 共通部品外の行をピンポイント出力
			String openLocation = "?";
			StackTraceElement[] stacktraces = new Throwable().getStackTrace();
			for (int i = 0; i < stacktraces.length; i++) {
				String clazz = stacktraces[i].getClassName();
				if (clazz.startsWith("java.lang.") ||
						clazz.startsWith("jp.co.webtechnology.dp.sql.")) {
					continue;
				}

				openLocation = stacktraces[i].toString();
				break;
			}
			logger.trace("open[" + this.conn.getClass().getName() + "@" + this.conn.hashCode() + "," + this.databaseType.toString() + "] at " + openLocation);
		}
	}

	/**
	 * データベースの種類を返します。
	 * @return データベースの種類
	 */
	DatabaseType getDatabaseType() {
		return this.databaseType;
	}

	/**
	 * データアクセスオブジェクトの接尾辞を設定します。
	 * @param daoSuffix データアクセスオブジェクトの接尾辞
	 */
	void setPersonalDaoSuffix(String daoSuffix) {
		this.personalDaoSuffix = daoSuffix;
	}

	/**
	 * データアクセスオブジェクトの接尾辞を返します。
	 * @return データアクセスオブジェクトの接尾辞
	 */
	String getPersonalDaoSuffix() {
		return this.personalDaoSuffix;
	}

	/** {@inheritDoc} */
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return new PreparedStatementWrapper(this.conn, sql, this.databaseType);
	}

	/** {@inheritDoc} */
	public void commit() throws SQLException {
		if (logger.isTraceEnabled()) {
			logger.trace("commit[@" + this.conn.hashCode() + "]");
		}

		this.conn.commit();
	}

	/** {@inheritDoc} */
	public void rollback() throws SQLException {
		if (logger.isTraceEnabled()) {
			logger.trace("rollback[@" + this.conn.hashCode() + "]");
		}

		this.conn.rollback();
	}

	/** {@inheritDoc} */
	public void close() throws SQLException {
		this.conn.close();
		this.isClosed = true;
	}

	/**
	 * ファイナライズ処理を実行します。
	 * リソースの解放漏れを検出して警告ログを出力します。
	 *
	 * @throws Throwable 処理中にエラーが発生した場合
	 */
	protected void finalize() throws Throwable {
		try {
			if (!this.isClosed) {
				logger.debug("LEAK![@" + this.conn.hashCode() + "]");
				this.close();
			}
		} finally {
			super.finalize();
		}
	}

	// 以降、使用想定外

	/** {@inheritDoc} */
	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Statement createStatement() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public String nativeSQL(String sql) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public boolean getAutoCommit() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public boolean isClosed() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setReadOnly(boolean readOnly) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public boolean isReadOnly() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setCatalog(String catalog) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public String getCatalog() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setTransactionIsolation(int level) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public int getTransactionIsolation() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void clearWarnings() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType,
			int resultSetConcurrency) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public CallableStatement prepareCall(String sql, int resultSetType,
			int resultSetConcurrency) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setHoldability(int holdability) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public int getHoldability() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Savepoint setSavepoint() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Savepoint setSavepoint(String name) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void rollback(Savepoint savepoint) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Statement createStatement(int resultSetType,
			int resultSetConcurrency, int resultSetHoldability)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType,
			int resultSetConcurrency, int resultSetHoldability)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public CallableStatement prepareCall(String sql, int resultSetType,
			int resultSetConcurrency, int resultSetHoldability)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public PreparedStatement prepareStatement(String sql, int[] columnIndexes)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public PreparedStatement prepareStatement(String sql, String[] columnNames)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Clob createClob() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Blob createBlob() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public NClob createNClob() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public SQLXML createSQLXML() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(int timeout) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setClientInfo(String name, String value)
			throws SQLClientInfoException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setClientInfo(Properties properties)
			throws SQLClientInfoException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public String getClientInfo(String name) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Properties getClientInfo() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Array createArrayOf(String typeName, Object[] elements)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public Struct createStruct(String typeName, Object[] attributes)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setSchema(String schema) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public String getSchema() throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void abort(Executor executor) throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

	/** {@inheritDoc} */
	@Override
	public int getNetworkTimeout() throws SQLException {
		throw new UnsupportedOperationException();
	}

}
